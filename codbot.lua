-- codbot
-- version 1.0
-- by gerrit maritz

local base = _G

local BROADCAST = "255.255.255.255"
local PORT = 28960
local query = string.format("%c%c%c%cgetinfo xxx",0xff,0xff,0xff,0xff)

module("codbot")
base.require('luadchpp')
local adchpp = base.luadchpp

base.assert(base['access'], 'access.lua must be loaded before codbot.lua')
local access = base.access

local os = base.require('os')
local io = base.require('io')
local string = base.require('string')
local autil = base.require('autil')
local socket = base.require('socket')
local table = base.require('table')

local cm = adchpp.getCM()
local lm = adchpp.getLM()

local serverlist

function string:split(sSeparator, nMax, bRegexp)
  if not (sSeparator ~= '') then
    return
  end
  if not (nMax == nil or nMax >= 1) then
    return
  end

  local aRecord = {}

  if self:len() > 0 then
    local bPlain = not bRegexp
    nMax = nMax or -1

    local nField=1 nStart=1
    local nFirst,nLast = self:find(sSeparator, nStart, bPlain)
    while nFirst and nMax ~= 0 do
      aRecord[nField] = self:sub(nStart, nFirst-1)
      nField = nField+1
      nStart = nLast+1
      nFirst,nLast = self:find(sSeparator, nStart, bPlain)
      nMax = nMax-1
    end
    aRecord[nField] = self:sub(nStart)
  end

  return aRecord
end

function broadcast(message)
  local entities = cm:getEntities()
  local size = entities:size()
  if size == 0 then
    return
  end

  local mass_msg = adchpp.AdcCommand(adchpp.AdcCommand_CMD_MSG, adchpp.AdcCommand_TYPE_INFO, adchpp.AdcCommand_HUB_SID)
  mass_msg:setFrom(adchpp.AdcCommand_HUB_SID)
  mass_msg:addParam(message)
  --mass_msg:addParam("PM", adchpp.AdcCommand_fromSID(mass_msg:getFrom()))

  for i = 0, size - 1 do
    local user = entities[i]:asClient()
    if user then
      mass_msg:setTo(user:getSID())
      user:send(mass_msg)
    end
  end
end

function parse_gametype(gametype)
  if gametype == "dm" then return "Free for All"
  elseif gametype == "koth" then return "Headquaters"
  elseif gametype == "sab" then return "Sabotage"
  elseif gametype == "sd" then return "Search and Destroy"
  elseif gametype == "war" then return "Team Deathmatch" end
  return ""
end

function parse_server(raw, from, port)
  local elements = string.split(raw, "\\")
  local n = table.getn(elements)
  
  local hostname
  local mapname
  local gametype
  local players = 0
  
  for i = 1,n do
    if     elements[i] == "hostname" then hostname = elements[i+1] 
	elseif elements[i] == "mapname"  then mapname  = elements[i+1] 
	elseif elements[i] == "clients"  then players  = elements[i+1]
	elseif elements[i] == "gametype" then gametype = parse_gametype(elements[i+1])
										  break
										  end
  end

  local msg = "Server Name: "..hostname.." - IP: "..from..":"..port.." - Map: "..mapname.." - Game: "..gametype.." - Players: "..players
  --lm.log(lm, "cod-server", msg)
  if base.tonumber(players) >= 4 then
    broadcast("CoD4 Game: "..msg)
  end
  table.insert(serverlist,msg)
  
end

function scan_servers()		     --  ff  ff   ff   ff   getinfo xxx
  local udp = socket.udp()
  udp:setoption('broadcast',true)
  --udp:setoption('dontroute',true)
  udp:settimeout(1)
  udp:setsockname('*',PORT)
  
  udp:sendto(query, BROADCAST, PORT)
  udp:sendto(query, BROADCAST, PORT+1)
  udp:sendto(query, BROADCAST, PORT+2)
  udp:sendto(query, BROADCAST, PORT+3)
  --lm.log(lm, "cod", 'sent')
  while true do
    local data = nil
	local from  = nil
	local port = nil
    --lm.log(lm, "cod", 'receiving')
	--udp:settimeout(1)
    data,from,port = udp:receivefrom()
	if data == nil and from == 'timeout' then
	  --lm.log(lm, "cod", 'timed out')
	  break
	end
	if data == nil and from == 'refused' then
	  lm.log(lm, "cod-error", 'refused')
	  break
	end
	if data ~= nil then
      --lm.log(lm, "cod-data", data)
	  if data ~= query then
	    parse_server(data, from, port)
	  end
	end
  end
  
  udp:close()
end



access.commands.cod = {
  alias = { cod = true },

  command = function(c, parameters)
            serverlist = {}
            local resp = scan_servers()
            -- local data  = string.split(temp, "<br />")
            --lm.log(lm, "cod", resp)
            local n = table.getn(serverlist)
			if n == 0 then
			  autil.reply(c, "Couldn't find any Call of Duty servers, sorry.")
			else
			  for i = 1,n do
				autil.reply(c, serverlist[i])
			  end
			end
            --autil.reply(c, resp)
  end,

  help = "- displays current weather",

  user_command = {
    name = "Cod",
    params = {}
  }
            
}
