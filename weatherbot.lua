-- weatherbot
-- version 1.0
-- by gerrit maritz

local apiurl = "http://weather.sun.ac.za/api/getlivedata.php"

local base = _G

module("weatherbot")
base.require('luadchpp')
local adchpp = base.luadchpp

base.assert(base['access'], 'access.lua must be loaded before weatherbot.lua')
local access = base.access

local os = base.require('os')
local io = base.require('io')
local string = base.require('string')
local autil = base.require('autil')
local http = base.require('socket.http')

local cm = adchpp.getCM()
local lm = adchpp.getLM()

function string:split(sSeparator, nMax, bRegexp)
  if not (sSeparator ~= '') then
    return
  end
  if not (nMax == nil or nMax >= 1) then
    return
  end

  local aRecord = {}

  if self:len() > 0 then
    local bPlain = not bRegexp
    nMax = nMax or -1

    local nField=1 nStart=1
    local nFirst,nLast = self:find(sSeparator, nStart, bPlain)
    while nFirst and nMax ~= 0 do
      aRecord[nField] = self:sub(nStart, nFirst-1)
      nField = nField+1
      nStart = nLast+1
      nFirst,nLast = self:find(sSeparator, nStart, bPlain)
      nMax = nMax-1
    end
    aRecord[nField] = self:sub(nStart)
  end

  return aRecord
end


access.commands.weather = {
  alias = { weather = true },

  command = function(c, parameters)
            local temp  = http.request(apiurl.."?temperature&maxtemp&maxtemptime&mintemp&mintemptime&humidity&baro&windavg&windavgdirsec&dayrain&quickforecast")
            local data  = string.split(temp, "<br />")
            --lm.log(lm, "weather", temp)
            local reply = "\nWeather Report:\n".."Current Temperature: "..data[1].."\194\176C\n".."Maximum Temperature: "..data[2].."\194\176C at "..data[3].."\nMinimum Temperature: "..data[4].."\194\176C at "..data[5].."\nHumidity: "..data[6].."%\nBarometric Pressure: "..data[7].." hPa\nAverage Windspeed: "..data[8].." km\\h\nAverage Wind Direction: "..data[9].."\nDaily Rain: "..data[10].." mm\nForecast: "..data[11]

            autil.reply(c, reply)
  end,

  help = "- displays current weather",

  user_command = {
    name = "Weather",
    params = {}
  }
            
}
